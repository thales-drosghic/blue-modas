﻿using AutoMapper;
using BlueModas.Pedidos.Comum;
using BlueModas.Pedidos.Dominio.Entidades;
using BlueModas.Pedidos.Dominio.Respostas;

namespace BlueModas.Pedidos.Infra
{
    public class Mapeador
    {
        public static void Mapear()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Pedido, PedidoRetorno>();
                cfg.CreateMap<Pedido, PedidoCadastradoRetorno>();
            });

            AutoMapperConfiguration.CriarMapeador(config);
        }
    }
}
