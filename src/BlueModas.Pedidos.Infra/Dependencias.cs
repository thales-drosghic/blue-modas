﻿using BlueModas.Clientes.Dominio.CasosDeUso.ListarPedidos;
using BlueModas.Pedidos.Dados.Repositorios;
using BlueModas.Pedidos.Dominio.CasosDeUso.CadastrarPedido;
using BlueModas.Pedidos.Dominio.CasosDeUso.PesquisarPedido;
using BlueModas.Pedidos.Dominio.Interfaces;
using SimpleInjector;

namespace BlueModas.Pedidos.Infra
{
    public class Dependencias
    {
        public static void Resolver(Container container)
        {
            container.Register<RgrCadastrarPedido>();
            container.Register<RgrListarPedidos>();
            container.Register<RgrPesquisarPedido>();
            container.Register<IPedidoRepository, PedidoRepository>();
        }
    }
}
