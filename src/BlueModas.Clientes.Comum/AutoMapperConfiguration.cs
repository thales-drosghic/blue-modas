﻿using AutoMapper;

namespace BlueModas.Clientes.Comum
{
    public static class AutoMapperConfiguration
    {
        public static IMapper Mapper { get; private set; }

        public static MapperConfiguration Configuracao { get; private set; }

        public static void CriarMapeador(MapperConfiguration config)
        {
            Configuracao = config;
            Mapper = config.CreateMapper();
        }
    }
}
