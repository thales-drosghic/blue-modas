﻿using AutoMapper;
using BlueModas.Clientes.Comum;
using BlueModas.Clientes.Dominio.Entidades;
using BlueModas.Clientes.Dominio.Respostas;

namespace BlueModas.Clientes.Infra
{
    public static class Mapeador
    {
        public static void Mapear()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Cliente, ClienteRetorno>();
                cfg.CreateMap<Cliente, ClienteCadastradoRetorno>();
            });

            AutoMapperConfiguration.CriarMapeador(config);
        }
    }
}
