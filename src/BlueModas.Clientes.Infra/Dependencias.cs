﻿using BlueModas.Clientes.Dados.Repositorios;
using BlueModas.Clientes.Dominio.CasosDeUso.CadastrarCliente;
using BlueModas.Clientes.Dominio.CasosDeUso.ListarClientes;
using BlueModas.Clientes.Dominio.Interfaces;
using SimpleInjector;

namespace BlueModas.Clientes.Infra
{
    public class Dependencias
    {
        public static void Resolver(Container container)
        {
            container.Register<RgrCadastrarCliente>();
            container.Register<RgrListarClientes>();
            container.Register<IClienteRepository, ClienteRepository>();
        }
    }
}
