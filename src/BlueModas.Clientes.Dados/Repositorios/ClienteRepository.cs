﻿using BlueModas.Clientes.Dominio.Entidades;
using BlueModas.Clientes.Dominio.Interfaces;
using System.Collections.Generic;

namespace BlueModas.Clientes.Dados.Repositorios
{
    public class ClienteRepository : IClienteRepository
    {
        public bool Existe(string email)
        {
            return Banco.Clientes.Exists(x => x.Email == email);
        }

        public void Inserir(Cliente cliente)
        {
            Banco.Clientes.Add(cliente);
        }

        public List<Cliente> Listar()
        {
            return Banco.Clientes;
        }
    }
}
