﻿using BlueModas.Clientes.Dominio.Entidades;
using BlueModas.Clientes.Dominio.Entidades.Modeladas;
using System;
using System.Collections.Generic;

namespace BlueModas.Clientes.Dados
{
    public static class Banco
    {
        public static List<Cliente> Clientes = new List<Cliente>() { new ClienteCadastro(new Guid("89ea9348-c3e2-4adb-a82e-f34b561b29d3"), "Jéssica", "jessica@gmail.com", "31988504719") };
    }
}
