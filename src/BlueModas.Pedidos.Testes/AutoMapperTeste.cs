﻿using BlueModas.Pedidos.Comum;
using NUnit.Framework;

namespace BlueModas.Pedidos.Testes
{
    class AutoMapperTeste : TesteBase
    {
        [Test]
        public void VerificaSeConfiguracaoDoAutoMapperEstaValido()
        {
            AutoMapperConfiguration.Configuracao.AssertConfigurationIsValid();
        }
    }
}
