﻿using BlueModas.Pedidos.Dominio.CasosDeUso.PesquisarPedido;
using BlueModas.Pedidos.Dominio.Entidades;
using BlueModas.Pedidos.Dominio.Interfaces;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;

namespace BlueModas.Pedidos.Testes.CasosDeUso.PesquisarPedido
{
    class RgrPesquisarPedidoTeste : TesteBase
    {
        private Guid id;
        private Mock<IPedidoRepository> repository;
        private RgrPesquisarPedido regra;

        [SetUp]
        public void Init()
        {
            this.id = new Guid("d1b4bd70-9f5c-4e04-a91d-3a16de612a58");
            this.repository = new Mock<IPedidoRepository>();
            this.regra = new RgrPesquisarPedido(this.repository.Object);
        }

        [Test]
        public void QuandoNaoEncontraPedidos_RetornaValidacao()
        {
            var pedido = this.regra.Pesquisar(this.id);

            this.regra.Valid.Should().BeFalse();
            this.regra.Notifications.Select(x => x.Message).Should().Contain("Nenhum pedido encontrado com o identificador informado.");
            pedido.Should().BeNull();
        }

        [Test]
        public void QuandoEncontraPedido_RetornaObjeto()
        {
            this.repository.Setup(x => x.PesquisarPedido(this.id)).Returns(new Pedido(this.id, 55));

            var pedido = this.regra.Pesquisar(this.id);
            this.regra.Valid.Should().BeTrue();
            pedido.Should().NotBeNull();
        }
    }
}