﻿using BlueModas.Carrinhos.Dominio.Entidades;
using System;
using System.Collections.Generic;

namespace BlueModas.Carrinhos.Dominio.Interfaces
{
    public interface ICarrinhoRepository
    {
        public void Inserir(Carrinho carrinho);
        public void Atualizar(Carrinho carrinho);
        public Carrinho Pesquisar(Guid id);
        public List<Carrinho> Listar();
    }
}
