﻿using BlueModas.Carrinhos.Comum;
using BlueModas.Carrinhos.Dominio.Interfaces;
using BlueModas.Carrinhos.Dominio.Respostas;
using BlueModas.Comum.Dominio;
using System.Collections.Generic;

namespace BlueModas.Carrinhos.Dominio.CasosDeUso.ListarCarrinhos
{
    public class RgrListarCarrinhos : RegraBase
    {
        private readonly ICarrinhoRepository repository;

        public RgrListarCarrinhos(ICarrinhoRepository repository)
        {
            this.repository = repository;
        }

        public List<CarrinhoRetorno> Listar()
        {
            var carrinhos = this.repository.Listar();

            return carrinhos.MapTo<List<CarrinhoRetorno>>();
        }
    }
}
