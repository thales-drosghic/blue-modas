﻿using System;

namespace BlueModas.Carrinhos.Dominio.CasosDeUso.AlterarProduto
{
    public class AlteraProdutoCommand
    {
        public Guid IdCarrinho { get; set; }
        public Guid IdProduto { get; set; }
        public int Quantidade { get; set; }
    }
}
