﻿using BlueModas.Carrinhos.Comum;
using BlueModas.Carrinhos.Dominio.Interfaces;
using BlueModas.Carrinhos.Dominio.Respostas;
using BlueModas.Comum.Dominio;
using System.Linq;

namespace BlueModas.Carrinhos.Dominio.CasosDeUso.AlterarProduto
{
    public class RgrAlterarProduto : RegraBase
    {
        private readonly ICarrinhoRepository repository;

        public RgrAlterarProduto(ICarrinhoRepository repository)
        {
            this.repository = repository;
        }

        public CarrinhoRetorno Alterar(AlteraProdutoCommand command)
        {
            var carrinho = this.repository.Pesquisar(command.IdCarrinho);
            if (carrinho == null)
            {
                this.AddNotification("Carrinho", "Carrinho não encontrado.");
                return null;
            }

            var produto = carrinho.Produtos.FirstOrDefault(x => x.IdProduto == command.IdProduto);
            if (produto == null)
            {
                this.AddNotification("Carrinho", "Produto não encontrado.");
                return null;
            }

            produto.AlterarQuantidade(command.Quantidade);
            this.repository.Atualizar(carrinho);

            return carrinho.MapTo<CarrinhoRetorno>();
        }
    }
}
