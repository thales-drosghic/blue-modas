﻿using BlueModas.Carrinhos.Comum;
using BlueModas.Carrinhos.Dominio.Entidades;
using BlueModas.Carrinhos.Dominio.Interfaces;
using BlueModas.Carrinhos.Dominio.Respostas;
using BlueModas.Comum.Dominio;
using System;

namespace BlueModas.Carrinhos.Dominio.CasosDeUso.PesquisarCarrinho
{
    public class RgrPesquisarCarrinho : RegraBase
    {
        private readonly ICarrinhoRepository repository;

        public RgrPesquisarCarrinho(ICarrinhoRepository repository)
        {
            this.repository = repository;
        }

        public CarrinhoRetorno Pesquisar(Guid id)
        {
            var carrinho = this.repository.Pesquisar(id);
            if (carrinho == null)
            {
                this.AddNotification("Carrinho", "Carrinho não encontrado.");
                return null;
            }

            return carrinho.MapTo<CarrinhoRetorno>();
        }
    }
}
