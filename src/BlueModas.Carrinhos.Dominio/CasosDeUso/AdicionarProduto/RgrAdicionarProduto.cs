﻿using BlueModas.Carrinhos.Comum;
using BlueModas.Carrinhos.Dominio.Entidades;
using BlueModas.Carrinhos.Dominio.Interfaces;
using BlueModas.Carrinhos.Dominio.Respostas;
using BlueModas.Comum.Dominio;

namespace BlueModas.Carrinhos.Dominio.CasosDeUso.AdicionarProduto
{
    public class RgrAdicionarProduto : RegraBase
    {
        private readonly ICarrinhoRepository repository;

        public RgrAdicionarProduto(ICarrinhoRepository repository)
        {
            this.repository = repository;
        }

        public CarrinhoRetorno Adicionar(AdicionaProdutoCommand command)
        {
            Carrinho carrinho;
            if (command.IdCarrinho.HasValue)
            {
                carrinho = this.repository.Pesquisar(command.IdCarrinho.Value);
                if (carrinho == null)
                {
                    this.AddNotification("Carrinho", "Carrinho não encontrado.");
                    return null;
                }
            }
            else
            {
                carrinho = new Carrinho();
                this.repository.Inserir(carrinho);
            }

            carrinho.AdicionarProduto(new ProdutoCarrinho(carrinho.Id, command.IdProduto, 1, command.Preco));
            this.repository.Atualizar(carrinho);

            return carrinho.MapTo<CarrinhoRetorno>();
        }
    }
}
