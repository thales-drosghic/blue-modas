﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlueModas.Carrinhos.Dominio.CasosDeUso.AdicionarProduto
{
    public class AdicionaProdutoCommand
    {
        public Guid? IdCarrinho { get; set; }
        public Guid IdProduto { get; set; }
        public decimal Preco { get; set; }
    }
}
