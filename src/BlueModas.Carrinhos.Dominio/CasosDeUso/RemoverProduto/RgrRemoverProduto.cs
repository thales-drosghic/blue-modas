﻿using BlueModas.Carrinhos.Comum;
using BlueModas.Carrinhos.Dominio.Entidades;
using BlueModas.Carrinhos.Dominio.Interfaces;
using BlueModas.Carrinhos.Dominio.Respostas;
using BlueModas.Comum.Dominio;
using System;

namespace BlueModas.Carrinhos.Dominio.CasosDeUso.RemoverProduto
{
    public class RgrRemoverProduto : RegraBase
    {
        private readonly ICarrinhoRepository repository;

        public RgrRemoverProduto(ICarrinhoRepository repository)
        {
            this.repository = repository;
        }

        public CarrinhoRetorno Remover(Guid idCarrinho, Guid idProduto)
        {
            var carrinho = this.repository.Pesquisar(idCarrinho);
            if (carrinho == null)
            {
                this.AddNotification("Carrinho", "Carrinho não encontrado.");
                return null;
            }

            carrinho.RemoverProduto(idProduto);
            this.repository.Atualizar(carrinho);
            return carrinho.MapTo<CarrinhoRetorno>();
        }
    }
}
