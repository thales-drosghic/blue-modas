﻿using BlueModas.Comum.Dominio;
using System;
using System.Collections.Generic;

namespace BlueModas.Carrinhos.Dominio.Respostas
{
    public class CarrinhoRetorno : Response
    {
        public Guid Id { get; set; }
        public List<ProdutoCarrinhoRetorno> Produtos { get; protected set; }
    }
}
