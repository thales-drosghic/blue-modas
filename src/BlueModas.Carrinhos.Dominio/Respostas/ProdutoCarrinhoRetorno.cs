﻿using BlueModas.Comum.Dominio;
using System;

namespace BlueModas.Carrinhos.Dominio.Respostas
{
    public class ProdutoCarrinhoRetorno : Response
    {
        public Guid Id { get; set; }
        public Guid IdCarrinho { get; set; }
        public Guid IdProduto { get; set; }
        public int Quantidade { get; set; }
        public decimal ValorUnitario { get; set; }
    }
}
