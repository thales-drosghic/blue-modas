﻿using System;
using System.Collections.Generic;

namespace BlueModas.Carrinhos.Dominio.Entidades.Modeladas
{
    public class CarrinhoCadastro : Carrinho
    {
        public CarrinhoCadastro(Guid id, List<ProdutoCarrinho> produtos) : base()
        {
            this.Id = id;
            this.Produtos = produtos;
        }
    }
}
