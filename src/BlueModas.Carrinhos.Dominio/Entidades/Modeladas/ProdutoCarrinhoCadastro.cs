﻿using System;

namespace BlueModas.Carrinhos.Dominio.Entidades.Modeladas
{
    public class ProdutoCarrinhoCadastro : ProdutoCarrinho
    {
        public ProdutoCarrinhoCadastro(Guid id, Guid idCarrinho, Guid idProduto, int quantidade, decimal valorUnitario) : base(idCarrinho, idProduto, quantidade, valorUnitario)
        {
            this.Id = Id;
        }
    }
}
