﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BlueModas.Carrinhos.Dominio.Entidades
{
    public class Carrinho
    {
        public Carrinho()
        {
            this.Id = Guid.NewGuid();
            this.Produtos = new List<ProdutoCarrinho>();
        }

        public Guid Id { get; protected set; }
        public List<ProdutoCarrinho> Produtos { get; protected set; }

        public void AdicionarProduto(ProdutoCarrinho produto)
        {
            var produtoBanco = this.Produtos.FirstOrDefault(x => x.IdProduto == produto.IdProduto);
            if (produtoBanco == null)
            {
                this.Produtos.Add(produto);
            }
            else
            {
                produtoBanco.AlterarQuantidade(produtoBanco.Quantidade + 1);
            }
        }

        public void RemoverProduto(Guid id)
        {
            this.Produtos.RemoveAll(x => x.IdProduto == id);
        }
    }
}
