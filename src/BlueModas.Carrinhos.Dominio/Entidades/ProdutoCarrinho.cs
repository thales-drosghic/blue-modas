﻿using System;

namespace BlueModas.Carrinhos.Dominio.Entidades
{
    public class ProdutoCarrinho
    {
        public ProdutoCarrinho(Guid idCarrinho, Guid idProduto, int quantidade, decimal valorUnitario)
        {
            this.Id = Guid.NewGuid();
            this.IdCarrinho = idCarrinho;
            this.IdProduto = idProduto;
            this.Quantidade = quantidade;
            this.ValorUnitario = valorUnitario;
        }

        public Guid Id { get; protected set; }
        public Guid IdCarrinho { get; protected set; }
        public Guid IdProduto { get; protected set; }
        public int Quantidade { get; protected set; }
        public decimal ValorUnitario { get; protected set; }

        internal void AlterarQuantidade(int quantidade)
        {
            this.Quantidade = quantidade;
        }
    }
}
