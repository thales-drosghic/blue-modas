using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SimpleInjector;

namespace BlueModas.Api
{
    public class Startup
    {
        private Container container = new Container();
        private readonly string CorsAllowSpecificOrigins = "_corsAllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            this.container.Options.ResolveUnregisteredConcreteTypes = false;
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers(setupAction =>
            {
                setupAction.ReturnHttpNotAcceptable = true;
            }).AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.IgnoreNullValues = true;
            });

            services.AddSimpleInjector(this.container, options =>
            {
                options.AddAspNetCore().AddControllerActivation();
            });

            services.AddCors(options =>
            {
                options.AddPolicy(name: CorsAllowSpecificOrigins, builder =>
                {
                    builder.WithOrigins("http://localhost", "http://localhost:3000", "http://127.0.0.1")
                                        .AllowAnyHeader()
                                        .AllowAnyMethod();
                });
            });

            this.ConfigurarDominios(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSimpleInjector(this.container);
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("pt-BR"),
            });

            this.container.Verify();
        }

        private void ConfigurarDominios(IServiceCollection services)
        {
            Produtos.Infra.Dependencias.Resolver(this.container);
            Produtos.Infra.Mapeador.Mapear();
            Carrinhos.Infra.Dependencias.Resolver(this.container);
            Carrinhos.Infra.Mapeador.Mapear();
            Clientes.Infra.Dependencias.Resolver(this.container);
            Clientes.Infra.Mapeador.Mapear();
            Pedidos.Infra.Dependencias.Resolver(this.container);
            Pedidos.Infra.Mapeador.Mapear();
        }
    }
}
