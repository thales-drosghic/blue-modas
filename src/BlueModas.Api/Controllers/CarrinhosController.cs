﻿using BlueModas.Carrinhos.Dominio.CasosDeUso.AdicionarProduto;
using BlueModas.Carrinhos.Dominio.CasosDeUso.AlterarProduto;
using BlueModas.Carrinhos.Dominio.CasosDeUso.ListarCarrinhos;
using BlueModas.Carrinhos.Dominio.CasosDeUso.PesquisarCarrinho;
using BlueModas.Carrinhos.Dominio.CasosDeUso.RemoverProduto;
using Microsoft.AspNetCore.Mvc;
using System;

namespace BlueModas.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarrinhosController : BaseController
    {
        private readonly RgrAdicionarProduto rgrAdicionarProduto;
        private readonly RgrAlterarProduto rgrAlterarProduto;
        private readonly RgrListarCarrinhos rgrListarCarrinhos;
        private readonly RgrRemoverProduto rgrRemoverProduto;
        private readonly RgrPesquisarCarrinho rgrPesquisarCarrinho;

        public CarrinhosController(RgrAdicionarProduto rgrAdicionarProduto, RgrAlterarProduto rgrAlterarProduto, RgrListarCarrinhos rgrListarCarrinhos, RgrRemoverProduto rgrRemoverProduto, RgrPesquisarCarrinho rgrPesquisarCarrinho)
        {
            this.rgrAdicionarProduto = rgrAdicionarProduto;
            this.rgrAlterarProduto = rgrAlterarProduto;
            this.rgrListarCarrinhos = rgrListarCarrinhos;
            this.rgrRemoverProduto = rgrRemoverProduto;
            this.rgrPesquisarCarrinho = rgrPesquisarCarrinho;
        }

        [HttpGet]
        public ActionResult Get()
        {
            return Ok(this.rgrListarCarrinhos.Listar());
        }

        [HttpGet("{id}")]
        public ActionResult Get(Guid id)
        {
            var retorno = this.rgrPesquisarCarrinho.Pesquisar(id);
            return this.TratarRetorno(this.rgrPesquisarCarrinho, retorno);
        }

        [HttpPost]
        public ActionResult Post([FromBody] AdicionaProdutoCommand command)
        {
            var retorno = this.rgrAdicionarProduto.Adicionar(command);
            return this.TratarRetorno(this.rgrAdicionarProduto, retorno);
        }

        [HttpPut("{id}/produto")]
        public ActionResult Put(Guid id, [FromBody] AlteraProdutoCommand command)
        {
            command.IdCarrinho = id;
            var retorno = this.rgrAlterarProduto.Alterar(command);
            return this.TratarRetorno(this.rgrAlterarProduto, retorno);
        }

        [HttpDelete("{idCarrinho}/produto/{idProduto}")]
        public ActionResult Delete(Guid idCarrinho, Guid idProduto)
        {
            var retorno = this.rgrRemoverProduto.Remover(idCarrinho, idProduto);
            return this.TratarRetorno(this.rgrRemoverProduto, retorno);
        }
    }
}
