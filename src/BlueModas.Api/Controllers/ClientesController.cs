﻿using BlueModas.Clientes.Dominio.CasosDeUso.CadastrarCliente;
using BlueModas.Clientes.Dominio.CasosDeUso.ListarClientes;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BlueModas.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientesController : BaseController
    {
        private readonly RgrCadastrarCliente rgrCadastrarCliente;
        private readonly RgrListarClientes rgrListarClientes;

        public ClientesController(RgrCadastrarCliente rgrCadastrarCliente, RgrListarClientes rgrListarClientes)
        {
            this.rgrCadastrarCliente = rgrCadastrarCliente;
            this.rgrListarClientes = rgrListarClientes;
        }

        [HttpGet]
        public ActionResult Get()
        {
            var retorno = this.rgrListarClientes.Listar();
            return this.TratarRetorno(this.rgrListarClientes, retorno);
        }

        [HttpPost]
        public ActionResult Post([FromBody] CadastroClienteCommand command)
        {
            var retorno = this.rgrCadastrarCliente.Cadastrar(command);
            return this.TratarRetorno(this.rgrCadastrarCliente, retorno);
        }
    }
}
