﻿using BlueModas.Comum.Dominio;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace BlueModas.Api.Controllers
{
    public class BaseController : ControllerBase
    {
        protected ActionResult TratarRetorno<TResponse>(RegraBase regra, TResponse retorno) where TResponse : Response
        {
            if (regra.Invalid)
            {
                return BadRequest(regra.Notifications);
            }

            if (retorno == null)
            {
                return NoContent();
            }

            return Ok(retorno);
        }

        protected ActionResult TratarRetorno<TResponse>(RegraBase regra, List<TResponse> retornos) where TResponse : Response
        {
            if (regra.Invalid)
            {
                return BadRequest(regra.Notifications);
            }

            if (retornos == null || !retornos.Any())
            {
                return NoContent();
            }

            return Ok(retornos);
        }
    }
}
