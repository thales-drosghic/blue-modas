﻿using BlueModas.Produtos.Dominio.CasosDeUso.ListarProdutos;
using BlueModas.Produtos.Dominio.CasosDeUso.PesquisarProduto;
using Microsoft.AspNetCore.Mvc;
using System;

namespace BlueModas.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutosController : BaseController
    {
        private readonly RgrListarProdutos regraListarProdutos;
        private readonly RgrPesquisarProduto rgrPesquisarProduto;

        public ProdutosController(RgrListarProdutos regraListarProdutos, RgrPesquisarProduto rgrPesquisarProduto)
        {
            this.regraListarProdutos = regraListarProdutos;
            this.rgrPesquisarProduto = rgrPesquisarProduto;
        }

        [HttpGet]
        public ActionResult Get()
        {
            var retorno = this.regraListarProdutos.Listar();
            return this.TratarRetorno(this.regraListarProdutos, retorno);
        }

        [HttpGet("{id}")]
        public ActionResult Get(Guid id)
        {
            var retorno = this.rgrPesquisarProduto.PesquisarProduto(id);
            return this.TratarRetorno(this.rgrPesquisarProduto, retorno);
        }
    }
}
