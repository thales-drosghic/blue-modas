﻿using BlueModas.Clientes.Dominio.CasosDeUso.ListarPedidos;
using BlueModas.Pedidos.Dominio.CasosDeUso.CadastrarPedido;
using BlueModas.Pedidos.Dominio.CasosDeUso.PesquisarPedido;
using Microsoft.AspNetCore.Mvc;
using System;

namespace BlueModas.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidosController : BaseController
    {
        private readonly RgrCadastrarPedido rgrCadastrarPedido;
        private readonly RgrListarPedidos rgrListarPedidos;
        private readonly RgrPesquisarPedido rgrPesquisarPedido;

        public PedidosController(RgrCadastrarPedido rgrCadastrarPedido, RgrListarPedidos rgrListarPedidos, RgrPesquisarPedido rgrPesquisarPedido)
        {
            this.rgrCadastrarPedido = rgrCadastrarPedido;
            this.rgrListarPedidos = rgrListarPedidos;
            this.rgrPesquisarPedido = rgrPesquisarPedido;
        }

        [HttpGet]
        public ActionResult Get()
        {
            var retorno = this.rgrListarPedidos.Listar();
            return this.TratarRetorno(this.rgrListarPedidos, retorno);
        }

        [HttpGet("{id}")]
        public ActionResult Get(Guid id)
        {
            var retorno = this.rgrPesquisarPedido.Pesquisar(id);
            return this.TratarRetorno(this.rgrPesquisarPedido, retorno);
        }

        [HttpPost]
        public ActionResult Post([FromBody] CadastroPedidoCommand command)
        {
            var retorno = this.rgrCadastrarPedido.Cadastrar(command);
            return this.TratarRetorno(this.rgrCadastrarPedido, retorno);
        }
    }
}
