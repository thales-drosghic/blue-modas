﻿using Microsoft.AspNetCore.Mvc;

namespace BlueModas.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : Controller
    {
        [HttpGet]
        public string Get()
        {
            return "BlueModas Versão 1.0";
        }
    }
}
