﻿using BlueModas.Produtos.Integracao.Modelo;
using System;

namespace BlueModas.Produtos.Integracao
{
    public interface IProdutoIntegracao
    {
        public ProdutoIntegracaoRetorno PesquisarProduto(Guid id);
    }
}
