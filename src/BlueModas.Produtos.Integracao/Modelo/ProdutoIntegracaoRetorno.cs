﻿using BlueModas.Comum.Dominio;

namespace BlueModas.Produtos.Integracao.Modelo
{
    public class ProdutoIntegracaoRetorno : Response
    {
        public string Nome { get; set; }
        public decimal Preco { get; set; }
    }
}
