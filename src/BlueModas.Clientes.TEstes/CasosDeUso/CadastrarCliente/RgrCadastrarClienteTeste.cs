﻿using BlueModas.Clientes.Dominio.CasosDeUso.CadastrarCliente;
using BlueModas.Clientes.Dominio.Entidades;
using BlueModas.Clientes.Dominio.Interfaces;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Linq;

namespace BlueModas.Clientes.Testes.CasosDeUso.CadastrarCliente
{
    class RgrCadastrarClienteTeste : TesteBase
    {
        private CadastroClienteCommand command;
        private Mock<IClienteRepository> repository;
        private RgrCadastrarCliente regra;

        [SetUp]
        public void Init()
        {
            this.command = new CadastroClienteCommand() { Email = "thaleshfd@gmail.com", Nome = "Thales", Telefone = "31988504719" };
            this.repository = new Mock<IClienteRepository>();
            this.regra = new RgrCadastrarCliente(this.repository.Object);
        }

        [Test]
        public void QuandoEmailJaExiste_RetornaValidacao()
        {
            this.repository.Setup(x => x.Existe("thaleshfd@gmail.com")).Returns(true);
            var clientes = this.regra.Cadastrar(this.command);

            this.regra.Valid.Should().BeFalse();
            this.regra.Notifications.Select(x => x.Message).Should().Contain("E-mail já cadastrado, faça o login.");
            clientes.Should().BeNull();
        }

        [Test]
        public void QuandoDadosValidos_SalvaCliente()
        {
            this.command.Email = "novoemail@gmail.com";

            var clientes = this.regra.Cadastrar(this.command);
            this.regra.Valid.Should().BeTrue();
            this.repository.Verify(x => x.Inserir(It.IsAny<Cliente>()), Times.Once);
        }
    }
}
