﻿using BlueModas.Clientes.Dominio.CasosDeUso.ListarClientes;
using BlueModas.Clientes.Dominio.Entidades;
using BlueModas.Clientes.Dominio.Interfaces;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace BlueModas.Clientes.Testes.CasosDeUso.ListarClientes
{
    class RgrListarClientesTeste : TesteBase
    {
        private Mock<IClienteRepository> repository;
        private RgrListarClientes regra;

        [SetUp]
        public void Init()
        {
            this.repository = new Mock<IClienteRepository>();
            this.regra = new RgrListarClientes(this.repository.Object);
        }

        [Test]
        public void QuandoNaoEncontraClientes_RetornaValidacao()
        {
            var clientes = this.regra.Listar();

            this.regra.Valid.Should().BeTrue();
            clientes.Should().BeEmpty();
        }

        [Test]
        public void QuandoEncontraClientes_RetornaLista()
        {
            this.repository.Setup(x => x.Listar()).Returns(new List<Cliente>() { new Cliente("thales", "thaleshfd@gmail.com", "31988504719") });

            var clientes = this.regra.Listar();
            this.regra.Valid.Should().BeTrue();
            clientes.Should().HaveCount(1);
        }
    }
}
