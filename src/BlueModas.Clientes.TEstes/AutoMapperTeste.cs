﻿using BlueModas.Clientes.Comum;
using NUnit.Framework;

namespace BlueModas.Clientes.Testes
{
    class AutoMapperTeste : TesteBase
    {
        [Test]
        public void VerificaSeConfiguracaoDoAutoMapperEstaValido()
        {
            AutoMapperConfiguration.Configuracao.AssertConfigurationIsValid();
        }
    }
}
