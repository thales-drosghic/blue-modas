﻿using AutoMapper;
using BlueModas.Produtos.Comum;
using BlueModas.Produtos.Dominio.CasosDeUso.Entidades.Modeladas;
using BlueModas.Produtos.Dominio.Entidades;
using BlueModas.Produtos.Dominio.Respostas;
using BlueModas.Produtos.Integracao.Modelo;

namespace BlueModas.Produtos.Infra
{
    public static class Mapeador
    {
        public static void Mapear()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ProdutoCadastro, ProdutoRetorno>();
                cfg.CreateMap<Produto, ProdutoRetorno>();
                cfg.CreateMap<Produto, ProdutoIntegracaoRetorno>();
            });

            AutoMapperConfiguration.CriarMapeador(config);
        }
    }
}
