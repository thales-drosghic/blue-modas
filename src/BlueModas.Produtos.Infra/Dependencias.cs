﻿using BlueModas.Produtos.Dados.Repositorios;
using BlueModas.Produtos.Dominio.CasosDeUso.ListarProdutos;
using BlueModas.Produtos.Dominio.CasosDeUso.PesquisarProduto;
using BlueModas.Produtos.Dominio.Integracoes;
using BlueModas.Produtos.Dominio.Interfaces;
using BlueModas.Produtos.Integracao;
using SimpleInjector;

namespace BlueModas.Produtos.Infra
{
    public static class Dependencias
    {
        public static void Resolver(Container container)
        {
            container.Register<RgrListarProdutos>();
            container.Register<RgrPesquisarProduto>();
            container.Register<IProdutoIntegracao, ProdutoIntegracao>();
            container.Register<IProdutoRepository, ProdutoRepository>();
        }
    }
}
