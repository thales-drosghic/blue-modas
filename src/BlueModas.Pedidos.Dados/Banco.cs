﻿using BlueModas.Pedidos.Dominio.Entidades;
using BlueModas.Pedidos.Dominio.Entidades.Modeladas;
using System;
using System.Collections.Generic;

namespace BlueModas.Pedidos.Dados
{
    public static class Banco
    {
        public static List<Pedido> Pedidos = new List<Pedido>() { new PedidoCadastro(new Guid("c0098559-079c-4520-968d-45dcea843a35"), new Guid("89ea9348-c3e2-4adb-a82e-f34b561b29d3"), 1542, new List<ProdutoPedido>(){ new ProdutoPedido(new Guid("c0098559-079c-4520-968d-45dcea843a35"), new Guid("67263fb9-8a1e-41cb-83f6-26626c4e5167"), "Calça Jeans", 99.9m) })
        };
    }
}
