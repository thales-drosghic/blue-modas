﻿using BlueModas.Pedidos.Dominio.Entidades;
using BlueModas.Pedidos.Dominio.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlueModas.Pedidos.Dados.Repositorios
{
    public class PedidoRepository : IPedidoRepository
    {
        public Pedido PesquisarPedido(Guid id)
        {
            return Banco.Pedidos.FirstOrDefault(x => x.Id == id);
        }

        public List<Pedido> ListarPedidos()
        {
            return Banco.Pedidos;
        }

        public int UltimoPedido()
        {
            return Banco.Pedidos.Max(x => x.Numero);
        }

        public void Inserir(Pedido pedido)
        {
            Banco.Pedidos.Add(pedido);
        }
    }
}
