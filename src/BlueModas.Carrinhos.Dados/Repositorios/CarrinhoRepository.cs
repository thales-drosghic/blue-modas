﻿using BlueModas.Carrinhos.Dominio.Entidades;
using BlueModas.Carrinhos.Dominio.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlueModas.Carrinhos.Dados.Repositorios
{
    public class CarrinhoRepository : ICarrinhoRepository
    {
        public void Atualizar(Carrinho carrinho)
        {
            Banco.Carrinhos.RemoveAll(x => x.Id == carrinho.Id);
            Banco.Carrinhos.Add(carrinho);
        }

        public void Inserir(Carrinho carrinho)
        {
            Banco.Carrinhos.Add(carrinho);
        }

        public List<Carrinho> Listar()
        {
            return Banco.Carrinhos;
        }

        public Carrinho Pesquisar(Guid id)
        {
            return Banco.Carrinhos.FirstOrDefault(x => x.Id == id);
        }
    }
}
