﻿using BlueModas.Carrinhos.Dominio.Entidades;
using BlueModas.Carrinhos.Dominio.Entidades.Modeladas;
using System;
using System.Collections.Generic;

namespace BlueModas.Carrinhos.Dados
{
    public static class Banco
    {
        // Criado um carrinho já salvo no "banco" com dois produtos
        public static List<Carrinho> Carrinhos = new List<Carrinho>() {
            new CarrinhoCadastro(new Guid("50315780-449f-47aa-8d03-df28725cbead"),
                new List<ProdutoCarrinho>(){
                    new ProdutoCarrinho(new Guid("50315780-449f-47aa-8d03-df28725cbead"), new Guid("67263fb9-8a1e-41cb-83f6-26626c4e5167"), 1, 99.9m),
                    new ProdutoCarrinho(new Guid("50315780-449f-47aa-8d03-df28725cbead"), new Guid("804fbe32-ff30-4c13-bf31-1e0e93e14541"), 2, 159.9m)
                }) };
    };
}
