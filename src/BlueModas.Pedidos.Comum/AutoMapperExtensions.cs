﻿namespace BlueModas.Pedidos.Comum
{
    public static class AutoMapperExtensions
    {
        public static TDestino MapTo<TDestino>(this object origem)
        {
            return AutoMapperConfiguration.Mapper.Map<TDestino>(origem);
        }

        public static TDestino MapTo<TOrigem, TDestino>(this TOrigem origem, TDestino destino)
        {
            return AutoMapperConfiguration.Mapper.Map(origem, destino);
        }
    }
}
