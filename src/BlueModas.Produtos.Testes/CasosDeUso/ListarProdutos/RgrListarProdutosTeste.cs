﻿using BlueModas.Produtos.Dominio.CasosDeUso.ListarProdutos;
using BlueModas.Produtos.Dominio.Entidades;
using BlueModas.Produtos.Dominio.Interfaces;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace BlueModas.Produtos.Testes.CasosDeUso.ListarProdutos
{
    class RgrListarProdutosTeste : TesteBase
    {
        private Mock<IProdutoRepository> repository;
        private RgrListarProdutos regra;

        [SetUp]
        public void Init()
        {
            this.repository = new Mock<IProdutoRepository>();
            this.regra = new RgrListarProdutos(this.repository.Object);
        }

        [Test]
        public void QuandoNaoEncontraProdutos_RetornaValidacao()
        {
            var produtos = this.regra.Listar();

            this.regra.Valid.Should().BeTrue();
            produtos.Should().BeEmpty();
        }

        [Test]
        public void QuandoEncontraProdutos_RetornaLista()
        {
            this.repository.Setup(x => x.Listar()).Returns(new List<Produto>() { new Produto("calça", 10, "") });

            var produtos = this.regra.Listar();
            this.regra.Valid.Should().BeTrue();
            produtos.Should().HaveCount(1);
        }
    }
}
