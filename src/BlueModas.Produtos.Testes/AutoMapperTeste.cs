﻿using BlueModas.Produtos.Comum;
using NUnit.Framework;

namespace BlueModas.Produtos.Testes
{
    class AutoMapperTeste : TesteBase
    {
        [Test]
        public void VerificaSeConfiguracaoDoAutoMapperEstaValido()
        {
            AutoMapperConfiguration.Configuracao.AssertConfigurationIsValid();
        }
    }
}
