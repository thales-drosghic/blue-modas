﻿using BlueModas.Carrinhos.Dominio.CasosDeUso.PesquisarCarrinho;
using BlueModas.Carrinhos.Dominio.Entidades;
using BlueModas.Carrinhos.Dominio.Interfaces;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;

namespace BlueModas.Carrinhos.Testes.CasosDeUso.PesquisarCarrinho
{
    class RgrPesquisarCarrinhoTeste : TesteBase
    {
        private Guid id;
        private Mock<ICarrinhoRepository> repository;
        private RgrPesquisarCarrinho regra;

        [SetUp]
        public void Init()
        {
            this.id = new Guid("d1b4bd70-9f5c-4e04-a91d-3a16de612a58");
            this.repository = new Mock<ICarrinhoRepository>();
            this.regra = new RgrPesquisarCarrinho(this.repository.Object);
        }

        [Test]
        public void QuandoNaoEncontraCarrinhos_RetornaValidacao()
        {
            var carrinho = this.regra.Pesquisar(this.id);

            this.regra.Valid.Should().BeFalse();
            this.regra.Notifications.Select(x => x.Message).Should().Contain("Carrinho não encontrado.");
            carrinho.Should().BeNull();
        }

        [Test]
        public void QuandoEncontraCarrinho_RetornaObjeto()
        {
            this.repository.Setup(x => x.Pesquisar(this.id)).Returns(new Carrinho());

            var carrinho = this.regra.Pesquisar(this.id);
            this.regra.Valid.Should().BeTrue();
            carrinho.Should().NotBeNull();
        }
    }
}
