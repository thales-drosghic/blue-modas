﻿using BlueModas.Carrinhos.Dominio.CasosDeUso.RemoverProduto;
using BlueModas.Carrinhos.Dominio.Entidades;
using BlueModas.Carrinhos.Dominio.Interfaces;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlueModas.Carrinhos.Testes.CasosDeUso.RemoverProduto
{
    class RgrRemoverProdutoTeste : TesteBase
    {
        private Guid idCarrinho;
        private Mock<ICarrinhoRepository> repository;
        private RgrRemoverProduto regra;

        [SetUp]
        public void Init()
        {
            this.idCarrinho = new Guid("913278c2-9886-4265-96f6-d37a00a3f851");

            this.repository = new Mock<ICarrinhoRepository>();
            this.regra = new RgrRemoverProduto(this.repository.Object);
            this.repository.Setup(x => x.Pesquisar(this.idCarrinho)).Returns(new Carrinho());
        }

        [Test]
        public void QuandoNaoEncontraCarrinhos_RetornaValidacao()
        {
            var carrinhos = this.regra.Remover(Guid.NewGuid(), Guid.NewGuid());

            this.regra.Valid.Should().BeFalse();
            this.regra.Notifications.Select(x => x.Message).Should().Contain("Carrinho não encontrado.");
            carrinhos.Should().BeNull();
        }

        [Test]
        public void QuandoEncontraCarrinhos_RemoveProduto()
        {
            this.repository.Setup(x => x.Listar()).Returns(new List<Carrinho>() { new Carrinho() });
            var carrinho = this.regra.Remover(this.idCarrinho, Guid.NewGuid());
            this.repository.Verify(x => x.Atualizar(It.IsAny<Carrinho>()), Times.Once);
            this.regra.Valid.Should().BeTrue();
        }
    }
}
