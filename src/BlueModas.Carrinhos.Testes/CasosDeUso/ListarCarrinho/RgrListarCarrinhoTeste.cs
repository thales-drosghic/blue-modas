﻿using BlueModas.Carrinhos.Dominio.CasosDeUso.ListarCarrinhos;
using BlueModas.Carrinhos.Dominio.Entidades;
using BlueModas.Carrinhos.Dominio.Interfaces;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace BlueModas.Carrinhos.Testes.CasosDeUso.ListarCarrinho
{
    class RgrListarCarrinhoTeste : TesteBase
    {
        private Mock<ICarrinhoRepository> repository;
        private RgrListarCarrinhos regra;

        [SetUp]
        public void Init()
        {
            this.repository = new Mock<ICarrinhoRepository>();
            this.regra = new RgrListarCarrinhos(this.repository.Object);
        }

        [Test]
        public void QuandoNaoEncontraCarrinhos_RetornaListaVazia()
        {
            var carrinhos = this.regra.Listar();

            this.regra.Valid.Should().BeTrue();
            carrinhos.Should().BeEmpty();
        }

        [Test]
        public void QuandoEncontraCarrinhos_RetornaLista()
        {
            this.repository.Setup(x => x.Listar()).Returns(new List<Carrinho>() { new Carrinho() });

            var carrinhos = this.regra.Listar();
            this.regra.Valid.Should().BeTrue();
            carrinhos.Should().HaveCount(1);
        }
    }
}
