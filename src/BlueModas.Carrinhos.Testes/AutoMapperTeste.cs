﻿using BlueModas.Carrinhos.Comum;
using NUnit.Framework;

namespace BlueModas.Carrinhos.Testes
{
    class AutoMapperTeste : TesteBase
    {
        [Test]
        public void VerificaSeConfiguracaoDoAutoMapperEstaValido()
        {
            AutoMapperConfiguration.Configuracao.AssertConfigurationIsValid();
        }
    }
}
