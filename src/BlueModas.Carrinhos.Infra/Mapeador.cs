﻿using AutoMapper;
using BlueModas.Carrinhos.Comum;
using BlueModas.Carrinhos.Dominio.Entidades;
using BlueModas.Carrinhos.Dominio.Respostas;

namespace BlueModas.Carrinhos.Infra
{
    public static class Mapeador
    {
        public static void Mapear()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Carrinho, CarrinhoRetorno>();
                cfg.CreateMap<ProdutoCarrinho, ProdutoCarrinhoRetorno>();
            });

            AutoMapperConfiguration.CriarMapeador(config);
        }
    }
}
