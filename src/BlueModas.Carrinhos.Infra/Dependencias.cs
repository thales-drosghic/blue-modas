﻿using BlueModas.Carrinhos.Dados.Repositorios;
using BlueModas.Carrinhos.Dominio.CasosDeUso.AdicionarProduto;
using BlueModas.Carrinhos.Dominio.CasosDeUso.AlterarProduto;
using BlueModas.Carrinhos.Dominio.CasosDeUso.ListarCarrinhos;
using BlueModas.Carrinhos.Dominio.CasosDeUso.PesquisarCarrinho;
using BlueModas.Carrinhos.Dominio.CasosDeUso.RemoverProduto;
using BlueModas.Carrinhos.Dominio.Interfaces;
using SimpleInjector;

namespace BlueModas.Carrinhos.Infra
{
    public class Dependencias
    {
        public static void Resolver(Container container)
        {
            container.Register<RgrAdicionarProduto>();
            container.Register<RgrAlterarProduto>();
            container.Register<RgrListarCarrinhos>();
            container.Register<RgrPesquisarCarrinho>();
            container.Register<RgrRemoverProduto>();
            container.Register<ICarrinhoRepository, CarrinhoRepository>();
        }
    }
}
