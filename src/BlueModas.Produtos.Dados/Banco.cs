﻿using BlueModas.Produtos.Dominio.CasosDeUso.Entidades.Modeladas;
using BlueModas.Produtos.Dominio.Entidades;
using System;
using System.Collections.Generic;

namespace BlueModas.Produtos.Dados
{
    public static class Banco
    {
        // Criado o "banco" com produtos já cadastrados
        public static List<Produto> Produtos = new List<Produto>(){
                new ProdutoCadastro(new Guid("67263fb9-8a1e-41cb-83f6-26626c4e5167"), "Calça Jeans", 99.9m, "calcajeans.jpg"),
                new ProdutoCadastro(new Guid("804fbe32-ff30-4c13-bf31-1e0e93e14541"), "Blusa Moletom", 159.9m, "moletom.jpg"),
                new ProdutoCadastro(new Guid("8a9aeb94-80f5-4124-a640-d1befbe45dda"), "3 pares de Meia", 15.9m, "meia.jpg"),
                new ProdutoCadastro(new Guid("d8f93816-4e4e-4e9f-9e63-a1ee994d26ec"), "Camiseta Calvin Klein", 89.9m, "camisetack.jpg")
            };
    }
}
