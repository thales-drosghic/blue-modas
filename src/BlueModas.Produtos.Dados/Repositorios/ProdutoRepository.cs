﻿using BlueModas.Produtos.Dominio.Entidades;
using BlueModas.Produtos.Dominio.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlueModas.Produtos.Dados.Repositorios
{
    public class ProdutoRepository : IProdutoRepository
    {
        public List<Produto> Listar()
        {
            return Banco.Produtos;
        }

        public Produto PesquisarProduto(Guid id)
        {
            return Banco.Produtos.Where(x => x.Id == id).FirstOrDefault();
        }
    }
}
