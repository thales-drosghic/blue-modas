﻿using BlueModas.Comum.Dominio;
using BlueModas.Pedidos.Dominio.Entidades;
using System;
using System.Collections.Generic;

namespace BlueModas.Pedidos.Dominio.Respostas
{
    public class PedidoRetorno : Response
    {
        public Guid Id { get; set; }
        public Guid IdCliente { get; set; }
        public int Numero { get; set; }
        public List<ProdutoPedido> Produtos { get; set; }
    }
}
