﻿using BlueModas.Comum.Dominio;

namespace BlueModas.Pedidos.Dominio.Respostas
{
    public class PedidoCadastradoRetorno : Response
    {
        public int Numero { get; set; }
    }
}
