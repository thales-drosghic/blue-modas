﻿using BlueModas.Comum.Dominio;
using BlueModas.Pedidos.Comum;
using BlueModas.Pedidos.Dominio.Interfaces;
using BlueModas.Pedidos.Dominio.Respostas;
using System.Collections.Generic;

namespace BlueModas.Clientes.Dominio.CasosDeUso.ListarPedidos
{
    public class RgrListarPedidos : RegraBase
    {
        private readonly IPedidoRepository repository;

        public RgrListarPedidos(IPedidoRepository repository)
        {
            this.repository = repository;
        }

        public List<PedidoRetorno> Listar()
        {
            var pedidos = this.repository.ListarPedidos();

            return pedidos.MapTo<List<PedidoRetorno>>();
        }
    }
}
