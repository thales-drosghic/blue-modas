﻿using BlueModas.Comum.Dominio;
using BlueModas.Pedidos.Comum;
using BlueModas.Pedidos.Dominio.Interfaces;
using BlueModas.Pedidos.Dominio.Respostas;
using System;

namespace BlueModas.Pedidos.Dominio.CasosDeUso.PesquisarPedido
{
    public class RgrPesquisarPedido : RegraBase
    {
        private readonly IPedidoRepository repository;

        public RgrPesquisarPedido(IPedidoRepository repository)
        {
            this.repository = repository;
        }

        public PedidoRetorno Pesquisar(Guid id)
        {
            var produto = this.repository.PesquisarPedido(id);
            if (produto == null)
            {
                this.AddNotification("Pedido", "Nenhum pedido encontrado com o identificador informado.");
                return null;
            }

            return produto.MapTo<PedidoRetorno>();
        }
    }
}
