﻿using System;
using System.Collections.Generic;

namespace BlueModas.Pedidos.Dominio.CasosDeUso.CadastrarPedido
{
    public class CadastroPedidoCommand
    {
        public Guid IdCliente { get; set; }
        public List<ProdutoPedidoCommand> Produtos { get; set; }

        public class ProdutoPedidoCommand
        {
            public Guid Id { get; set; }
            public int Quantidade { get; set; }
        }
    }
}
