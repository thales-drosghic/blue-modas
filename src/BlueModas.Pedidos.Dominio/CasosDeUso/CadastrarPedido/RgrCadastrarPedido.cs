﻿using BlueModas.Comum.Dominio;
using BlueModas.Pedidos.Comum;
using BlueModas.Pedidos.Dominio.Entidades;
using BlueModas.Pedidos.Dominio.Interfaces;
using BlueModas.Pedidos.Dominio.Respostas;
using BlueModas.Produtos.Integracao;

namespace BlueModas.Pedidos.Dominio.CasosDeUso.CadastrarPedido
{
    public class RgrCadastrarPedido : RegraBase
    {
        private readonly IPedidoRepository repository;
        private readonly IProdutoIntegracao integracao;

        public RgrCadastrarPedido(IPedidoRepository repository, IProdutoIntegracao integracao)
        {
            this.repository = repository;
            this.integracao = integracao;
        }

        public PedidoCadastradoRetorno Cadastrar(CadastroPedidoCommand command)
        {
            var numero = this.repository.UltimoPedido() + 1;
            var pedido = new Pedido(command.IdCliente, numero);

            foreach (var produto in command.Produtos)
            {
                var prod = this.integracao.PesquisarProduto(produto.Id);

                for (int i = 0; i < produto.Quantidade; i++)
                {
                    pedido.AdicionarProduto(new ProdutoPedido(produto.Id, pedido.Id, prod.Nome, prod.Preco));
                }
            }

            this.repository.Inserir(pedido);
            return pedido.MapTo<PedidoCadastradoRetorno>();
        }
    }
}
