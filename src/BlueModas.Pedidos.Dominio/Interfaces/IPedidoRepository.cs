﻿using BlueModas.Pedidos.Dominio.Entidades;
using System;
using System.Collections.Generic;

namespace BlueModas.Pedidos.Dominio.Interfaces
{
    public interface IPedidoRepository
    {
        public Pedido PesquisarPedido(Guid id);
        public int UltimoPedido();
        public List<Pedido> ListarPedidos();
        public void Inserir(Pedido pedido);
    }
}
