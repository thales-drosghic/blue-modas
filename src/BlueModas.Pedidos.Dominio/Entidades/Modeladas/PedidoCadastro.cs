﻿using System;
using System.Collections.Generic;

namespace BlueModas.Pedidos.Dominio.Entidades.Modeladas
{
    public class PedidoCadastro : Pedido
    {
        public PedidoCadastro(Guid id, Guid idCliente, int numero, List<ProdutoPedido> produtos) : base(idCliente, numero)
        {
            this.Id = id;
            this.Produtos = produtos;
        }
    }
}
