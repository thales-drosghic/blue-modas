﻿using System;

namespace BlueModas.Pedidos.Dominio.Entidades
{
    public class ProdutoPedido
    {
        public ProdutoPedido(Guid idProduto, Guid idPedido, string nome, decimal valor)
        {
            this.Id = Guid.NewGuid();
            this.IdProduto = idProduto;
            this.IdPedido = idPedido;
            this.Nome = nome;
            this.Valor = valor;
        }

        public Guid Id { get; protected set; }
        public Guid IdProduto { get; protected set; }
        public Guid IdPedido { get; protected set; }
        public string Nome { get; protected set; }
        public decimal Valor { get; protected set; }
    }
}
