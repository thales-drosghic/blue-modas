﻿using System;
using System.Collections.Generic;

namespace BlueModas.Pedidos.Dominio.Entidades
{
    public class Pedido
    {
        public Pedido(Guid idCliente, int numero)
        {
            this.Id = Guid.NewGuid();
            this.IdCliente = idCliente;
            this.Numero = numero;
            this.Produtos = new List<ProdutoPedido>();
        }

        public Guid Id { get; protected set; }
        public Guid IdCliente { get; protected set; }
        public int Numero { get; protected set; }
        public List<ProdutoPedido> Produtos { get; protected set; }

        internal void AdicionarProduto(ProdutoPedido produtoPedido)
        {
            this.Produtos.Add(produtoPedido);
        }
    }
}
