﻿using BlueModas.Produtos.Comum;
using BlueModas.Produtos.Dominio.Interfaces;
using BlueModas.Produtos.Integracao;
using BlueModas.Produtos.Integracao.Modelo;
using System;

namespace BlueModas.Produtos.Dominio.Integracoes
{
    public class ProdutoIntegracao : IProdutoIntegracao
    {
        private readonly IProdutoRepository repository;

        public ProdutoIntegracao(IProdutoRepository repository)
        {
            this.repository = repository;
        }

        public ProdutoIntegracaoRetorno PesquisarProduto(Guid id)
        {
            var produto = this.repository.PesquisarProduto(id);
            return produto.MapTo<ProdutoIntegracaoRetorno>();
        }
    }
}
