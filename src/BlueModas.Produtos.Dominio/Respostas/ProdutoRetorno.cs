﻿using BlueModas.Comum.Dominio;
using System;

namespace BlueModas.Produtos.Dominio.Respostas
{
    public class ProdutoRetorno : Response
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public decimal Preco { get; set; }
        public string Imagem { get; set; }
    }
}
