﻿using BlueModas.Produtos.Dominio.Entidades;
using System;

namespace BlueModas.Produtos.Dominio.CasosDeUso.Entidades.Modeladas
{
    public class ProdutoCadastro : Produto
    {
        public ProdutoCadastro(Guid id, string nome, decimal preco, string imagem) : base(nome, preco, imagem)
        {
            this.Id = id;
        }
    }
}
