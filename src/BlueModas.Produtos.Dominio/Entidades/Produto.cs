﻿using System;

namespace BlueModas.Produtos.Dominio.Entidades
{
    public class Produto
    {
        public Produto(string nome, decimal preco, string imagem)
        {
            this.Id = Guid.NewGuid();
            this.Nome = nome;
            this.Preco = preco;
            this.Imagem = imagem;
        }

        public Guid Id { get; protected set; }
        public string Nome { get; protected set; }
        public decimal Preco { get; protected set; }
        public string Imagem { get; protected set; }
    }
}
