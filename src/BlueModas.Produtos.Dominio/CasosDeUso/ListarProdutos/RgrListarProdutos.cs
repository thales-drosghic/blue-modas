﻿using BlueModas.Comum.Dominio;
using BlueModas.Produtos.Comum;
using BlueModas.Produtos.Dominio.Interfaces;
using BlueModas.Produtos.Dominio.Respostas;
using System.Collections.Generic;

namespace BlueModas.Produtos.Dominio.CasosDeUso.ListarProdutos
{
    public class RgrListarProdutos : RegraBase
    {
        private readonly IProdutoRepository repository;

        public RgrListarProdutos(IProdutoRepository repository)
        {
            this.repository = repository;
        }

        public List<ProdutoRetorno> Listar()
        {
            var produtos = this.repository.Listar();

            return produtos.MapTo<List<ProdutoRetorno>>();
        }
    }
}
