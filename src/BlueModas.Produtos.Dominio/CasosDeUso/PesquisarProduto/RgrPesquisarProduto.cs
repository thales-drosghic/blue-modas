﻿using BlueModas.Comum.Dominio;
using BlueModas.Produtos.Comum;
using BlueModas.Produtos.Dominio.Interfaces;
using BlueModas.Produtos.Dominio.Respostas;
using System;

namespace BlueModas.Produtos.Dominio.CasosDeUso.PesquisarProduto
{
    public class RgrPesquisarProduto : RegraBase
    {
        private readonly IProdutoRepository repository;

        public RgrPesquisarProduto(IProdutoRepository repository)
        {
            this.repository = repository;
        }

        public ProdutoRetorno PesquisarProduto(Guid id)
        {
            var produto = this.repository.PesquisarProduto(id);
            if (produto == null)
            {
                this.AddNotification("Produto", "Nenhum produto encontrado com o identificador informado.");
                return null;
            }

            return produto.MapTo<ProdutoRetorno>();
        }
    }
}
