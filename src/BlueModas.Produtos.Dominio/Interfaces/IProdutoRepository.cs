﻿using BlueModas.Produtos.Dominio.Entidades;
using System;
using System.Collections.Generic;

namespace BlueModas.Produtos.Dominio.Interfaces
{
    public interface IProdutoRepository
    {
        public List<Produto> Listar();
        public Produto PesquisarProduto(Guid id);
    }
}
