﻿using BlueModas.Clientes.Dominio.Entidades;
using System.Collections.Generic;

namespace BlueModas.Clientes.Dominio.Interfaces
{
    public interface IClienteRepository
    {
        bool Existe(string email);
        void Inserir(Cliente cliente);
        List<Cliente> Listar();
    }
}
