﻿using System;

namespace BlueModas.Clientes.Dominio.Entidades.Modeladas
{
    public class ClienteCadastro : Cliente
    {
        public ClienteCadastro(Guid id, string nome, string email, string telefone) : base(nome, email, telefone)
        {
            this.Id = id;
        }
    }
}
