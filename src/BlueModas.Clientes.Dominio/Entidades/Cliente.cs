﻿using System;

namespace BlueModas.Clientes.Dominio.Entidades
{
    public class Cliente
    {
        public Cliente(string nome, string email, string telefone)
        {
            this.Id = Guid.NewGuid();
            this.Nome = nome;
            this.Email = email;
            this.Telefone = telefone;
        }

        public Guid Id { get; protected set; }
        public string Nome { get; protected set; }
        public string Email { get; protected set; }
        public string Telefone { get; protected set; }
    }
}
