﻿using BlueModas.Clientes.Comum;
using BlueModas.Clientes.Dominio.Entidades;
using BlueModas.Clientes.Dominio.Interfaces;
using BlueModas.Clientes.Dominio.Respostas;
using BlueModas.Comum.Dominio;

namespace BlueModas.Clientes.Dominio.CasosDeUso.CadastrarCliente
{
    public class RgrCadastrarCliente : RegraBase
    {
        private readonly IClienteRepository repository;

        public RgrCadastrarCliente(IClienteRepository repository)
        {
            this.repository = repository;
        }

        public ClienteCadastradoRetorno Cadastrar(CadastroClienteCommand command)
        {
            if (this.repository.Existe(command.Email))
            {
                this.AddNotification("Cliente", "E-mail já cadastrado, faça o login.");
                return null;
            }

            var cliente = new Cliente(command.Nome, command.Email, command.Telefone);
            this.repository.Inserir(cliente);
            return cliente.MapTo<ClienteCadastradoRetorno>();
        }
    }
}
