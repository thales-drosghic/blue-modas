﻿namespace BlueModas.Clientes.Dominio.CasosDeUso.CadastrarCliente
{
    public class CadastroClienteCommand
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}
