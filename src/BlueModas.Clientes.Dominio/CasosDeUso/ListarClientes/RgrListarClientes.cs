﻿using BlueModas.Clientes.Comum;
using BlueModas.Clientes.Dominio.Interfaces;
using BlueModas.Clientes.Dominio.Respostas;
using BlueModas.Comum.Dominio;
using System.Collections.Generic;

namespace BlueModas.Clientes.Dominio.CasosDeUso.ListarClientes
{
    public class RgrListarClientes : RegraBase
    {
        private readonly IClienteRepository repository;

        public RgrListarClientes(IClienteRepository repository)
        {
            this.repository = repository;
        }

        public List<ClienteRetorno> Listar()
        {
            var clientes = this.repository.Listar();

            return clientes.MapTo<List<ClienteRetorno>>();
        }
    }
}
