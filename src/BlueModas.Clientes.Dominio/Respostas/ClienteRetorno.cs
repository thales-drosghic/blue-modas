﻿using BlueModas.Comum.Dominio;
using System;

namespace BlueModas.Clientes.Dominio.Respostas
{
    public class ClienteRetorno : Response
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}
