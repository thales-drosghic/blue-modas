﻿using BlueModas.Comum.Dominio;
using System;

namespace BlueModas.Clientes.Dominio.Respostas
{
    public class ClienteCadastradoRetorno : Response
    {
        public Guid Id { get; set; }
    }
}
