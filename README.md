# blue-modas
## Projeto BlueModas
O Back-End está modularizado e seguindo os conceitos do solid. 

Foi pensado da seguinte forma, 4 módulos, produtos, carrinho, cliente e pedidos.

Contém somente uma api, mas possibilitando criar uma por módulo, virando microsserviços.

Os bancos de dados são classes estáticas, mas cada módulo pode usar a ferramenta de armazenamento preferida, no de carrinho por exemplo, eu usaria um banco nosql (mongo, dynamodb, etc) para rápida inserção e remoção.

Os módulos não conversam diretamente entre eles, para isso tem o projeto integração (no caso de microsserviços poderia ser utilizado mensageria).

**Não consegui iniciar o Front-End, mas todas as chamadas para testes da api podem ser importadas usando o arquivo dentro da pasta postman. Os testes unitários também estão todos rodando.**
**Segui mais a linha do caminho feliz, fiz algumas validações, mas não me preocupei muito com isso, quis mostrar no geral, a forma que eu gosto de codificar**

### Projetos:
- Dados - Contem o banco de dados e a implementação dos repositórios
- Domínio - Contém as regras de negócio do módulo
- Infra - Toda estrutura necessária do módulo (injeção de dependência, automapper, etc)
- Testes - Testes unitários, com NUnit, e Moq
- Api - Api seguindo o possível dos conceitos do rest.
- Compartilhado - Uma futura Lib, contém o que todos os módulos do projeto utilizam.
- Integração - Comunicação entre os módulos (no caso do microserviços seria utilizado mensageria)
